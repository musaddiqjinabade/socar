import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  View,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  StatusBar,
  StyleSheet,
  Platform,
  TouchableOpacity,
  Image,
  FlatList,
} from 'react-native';
import {Container, Toast} from 'native-base';
import Header from '../core/components/Header';
import styleConstants from '../styles/StyleConstants';
import commonStyles from '../styles/Common';
import LoadingIndicator from '../core/components/LoadingIndicator';
import Globals from '../Global';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
const axios = require('axios');
import database from '@react-native-firebase/database';

const Cars = () => {
  const [data, setData] = useState([]);
  const [expanded, setExpanded] = useState(true);
  const [agencyDetails, setAgencyDetails] = useState('');
  const [loading, setLoading] = useState(false);
  const [dateArray, setDateArray] = useState([]);
  const [totalCount, setTotalCount] = useState(0);
  const [oldpassword, setOldPassword] = useState('');
  const [newpassword, setNewPassword] = useState('');
  const [user_data, setUserData] = useState('');

  useEffect(() => {
    fetchData();
    fetchCarDetails();
  }, []);

  const fetchData = async () => {
    const data = await AsyncStorage.getItem('user_token').then((value) => {
      var userdata = JSON.parse(value);
      console.log('userdata::', userdata);
      if (userdata.BuyerId) {
        setUserData(userdata);
      }
    });
  };

  const fetchCarDetails = async () => {
    database()
      .ref('/users/Cars')
      .once('value')
      .then((snapshot) => {
        var data = [];
        snapshot.forEach((item) => {
          data.push(item);
        });
        setData(data);
      });
  };

  // showMessage(message){
  //   Toast.show({
  //     text: message,
  //     duration: 2000,
  //   });
  // }

  const renderItem = (items) => {
    const item = items.item;

    return (
      <TouchableOpacity
        key={items?.index}
        style={[commonStyles.full, commonStyles.row, {marginVertical: 10}]}>
        <View>
          <View
            style={{
                width: widthPercentageToDP('40%'),
                height: heightPercentageToDP('19%'),
                backgroundColor: '#f1f2f4',
                borderColor: '#c7c7c7',
                borderWidth: widthPercentageToDP('0.1%'),
                borderRadius: 5,
            }}>
            <View>
              <Image
                style={[styles.clientImage]}
                source={{uri: item?.car_img}}
              />
            </View>
          </View>
        </View>
        <View
          style={{
            marginLeft: widthPercentageToDP('3%'),
            // borderWidth: 1,
            flex: 1,
          }}>
          <Text
            style={{
              fontSize: heightPercentageToDP('2.2%'),
              color: '#293D68',
              fontFamily: 'Catamaran-Medium',
            }}>
            {item?.car_model}
          </Text>
          <Text
            style={{
              marginTop: -5,
              fontSize: heightPercentageToDP('1.8%'),
              color: '#828EA5',
              fontFamily: 'Catamaran-Medium',
            }}>
            {item?.car_model}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <SafeAreaView style={[commonStyles.full, commonStyles.backgroundColor]}>
      <Container>
        <StatusBar
          barStyle="light-content"
          hidden={false}
          backgroundColor="#transparent"
          translucent={true}
        />

        <Header
          onLogo={null}
          label={'Cars'}
          Agency_logo={agencyDetails}
          expanded={expanded}
        />
        <ScrollView>
          <KeyboardAvoidingView
            behavior={Platform.OS === 'ios' ? 'padding' : null}
            style={[commonStyles.full]}>
            <View style={[commonStyles.full, commonStyles.margin]}>
              <View
                style={[
                  styles.rectangleShape,
                  commonStyles.column,
                  {marginTop: heightPercentageToDP('2%')},
                ]}>
                <FlatList
                  data={data}
                  renderItem={renderItem}
                  keyExtractor={(item) => item.id}
                />
              </View>
            </View>
          </KeyboardAvoidingView>
        </ScrollView>
      </Container>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  rectangleShape: {
    padding: widthPercentageToDP('3%'),
    marginTop: widthPercentageToDP('3%'),
    borderWidth: 0.01,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
    borderTopRightRadius: 5,
    borderTopLeftRadius: 5,
    backgroundColor: '#fff',
    borderColor: '#fff',
  },
  clientImage: {
    flex: 1,
    aspectRatio: 1,
    borderColor: '#fff',
    backgroundColor: '#CFCFCF',
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
    borderTopRightRadius: 5,
    borderTopLeftRadius: 5,
  },
});
export default Cars;
