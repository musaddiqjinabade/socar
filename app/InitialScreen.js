import React, {useEffect, useState} from 'react';
import {SafeAreaView, StatusBar} from 'react-native';
import LoadingIndicator from './core/components/LoadingIndicator';
import styles from './styles/Common';
import TabManager from './home/TabManager';
import AsyncStorage from '@react-native-async-storage/async-storage';
import LoginScreen from './home/LoginScreen';

class InitialScreen extends React.Component {
  state = {
    loading: true,
    user_data: false,
  };

  componentDidMount() {
    // eslint-disable-next-line react/no-did-mount-set-state
    this.setState(
      {
        loading: true,
      },
      async () => {
        await AsyncStorage.getItem('user_token').then((value) => {
          var user_data = JSON.parse(value);
          console.log('userdata in ini', user_data);
          if (user_data != null) {
            this.setState({user_data: true, loading: false});
          } else {
            this.setState({user_data: false, loading: false});
          }
        });
      },
    );
  }

  getScreen() {
    if (this.state.loading) {
      return <LoadingIndicator />;
    } else {
      if (this.state.user_data) {
        return <TabManager />;
      } else {
        return <LoginScreen />;
      }
    }
  }

  render() {
    return (
      <SafeAreaView style={[styles.full]}>
        <StatusBar backgroundColor={'#000000'} barStyle="default" />
        {this.getScreen()}
      </SafeAreaView>
    );
  }
}

export default InitialScreen;

// // eslint-disable-next-line no-undef
// export default InitialScreen = () => {
//   const [loading, setLoading] = useState(true);
//   const [user_data, setUser_data] = useState(false);

//   // eslint-disable-next-line react-hooks/exhaustive-deps
//   useEffect(() => {
//     if (loading) {
//       AsyncStorage.getItem('user_token').then((value) => {
//         const userdata = JSON.parse(value);
//         if (userdata != null) {
//           setLoading(false);
//           setUser_data(true);
//         } else {
//           setLoading(false);
//           setUser_data(false);
//         }
//       });
//     }
//   }, [loading]);

//   const getScreen = () => {
//     if (loading) {
//       return <LoadingIndicator />;
//     } else {
//       if (user_data) {
//         return <TabManager />;
//       } else {
//         return <LoginScreen />;
//       }
//     }
//   };
//   return (
//     <SafeAreaView style={[styles.full]}>
//       <StatusBar backgroundColor={'#000000'} barStyle="default" />
//       {getScreen}
//     </SafeAreaView>
//   );
// };
