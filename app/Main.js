import React, {useEffect, useState} from 'react';
import {Root} from 'native-base';
import Navigator from './Navigator';
import {request, PERMISSIONS} from 'react-native-permissions';
import Geolocation from '@react-native-community/geolocation';
import {LogBox} from 'react-native';

const App = () => {
  const [userLocation, setUserLocation] = useState(false);

  useEffect(() => {
    LogBox.ignoreAllLogs();
    function getUserLocation() {
      requestLocationPermission();
    }
    getUserLocation();
  }, []);

  async function requestLocationPermission() {
    var response = await request(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION);
    if (response == 'granted') {
      await Geolocation.getCurrentPosition(
        ({coords}) => {
          setUserLocation(coords);
        },
        (error) => {
          console.log('error:', error);
        },
        {enableHighAccuracy: true, timeout: 20000, maximumAge: 15000},
      );
    }
  }
  // Geolocation.getCurrentPosition(info => console.log("location:",info));
  console.log('location', userLocation);
  return (
    <Root>
      <Navigator />
    </Root>
  );
};

export default App;
