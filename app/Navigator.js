import {createStackNavigator} from 'react-navigation-stack';
import {createAppContainer} from 'react-navigation';
import InitialScreen from './InitialScreen';
import Registration from './home/Registration';
import LoginScreen from './home/LoginScreen';
import DashboardScreen from './home/DashboardScreen';
import ChangePassword from './Profile/ChangePassword';
import ProfileScreen from './Profile/ProfileScreen';
import Cars from './Cars/index';

const Navigator = createStackNavigator(
  {
    InitialScreen,
    Registration,
    LoginScreen,
    DashboardScreen,
    ProfileScreen,
    ChangePassword,
    Cars,
  },
  {
    initialRouteName: 'InitialScreen',
    defaultNavigationOptions: {
      header: null,
    },
  },
);

export default createAppContainer(Navigator);
