import React from 'react';
import {
  SafeAreaView,
  Linking,
  Alert,
  View,
  Image,
  Dimensions,
  Text,
  ScrollView,
  TextInput,
  TouchableOpacity,
  Keyboard,
  KeyboardAvoidingView,
  Animated,
  StatusBar,
  ActivityIndicator,
  StyleSheet,
} from 'react-native';
import styles, {
  IMAGE_HEIGHT,
  IMAGE_HEIGHT_NEW,
  IMAGE_HEIGHT_SMALL,
} from '../styles/Common';
import styleConstants from '../styles/StyleConstants';
const WIDTH = Dimensions.get('screen').width;
const HEIGHT = Dimensions.get('screen').height;
import LinearGradient from 'react-native-linear-gradient';
import {
  withNavigation,
  StackActions,
  NavigationActions,
} from 'react-navigation';
import {Container, Toast} from 'native-base';
import Modal from 'react-native-modalbox';
import Header from '../core/components/Header';
import {
  widthPercentageToDP,
  heightPercentageToDP,
} from 'react-native-responsive-screen';

// import OfflineNotice from '../core/components/OfflineNotice'
// import NetInfo from "@react-native-community/netinfo";
import moment from 'moment';
import LoadingIndicator from '../core/components/LoadingIndicator';
import Globals from '../Global';
import AsyncStorage from '@react-native-async-storage/async-storage';
const axios = require('axios');

class ChangePassword extends React.Component {
  state = {
    data: '',
    expanded: true,
    loading: false,
    dateArray: [],
    totalCount: 0,
    oldpassword: '',
    Newpassword: '',
    user_data: '',
  };

  showMessage(message) {
    Toast.show({
      text: message,
      duration: 2000,
    });
  }

  checkInternet() {}

  async componentDidMount() {
    await AsyncStorage.getItem('user_token').then((value) => {
      var user_data = JSON.parse(value);
      console.log('userdata::', user_data);
      if (user_data.BuyerId) {
        this.setState({user_data: user_data});
      }
    });
  }

  _onRefresh = () => {
    this.setState({loading: true}, () => {
      // this.componentWillMount();
    });
  };

  ChangePassword() {
    this.setState({loading: true}, async () => {
      this.setState({loading: true});
      var url =
        Globals.domain +
        'ChangePassword?buyerid=' +
        this.state.user_data.BuyerId +
        '&OldPassword=' +
        this.state.oldpassword +
        '&NewPassword=' +
        this.state.Newpassword;
      console.log('sessions', url);
      const headers = {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      };
      try {
        var response = await axios.post(url, null, {
          headers: headers,
        });
        console.log('data : ', response);

        if (response.status == 200) {
          this.showMessage(response.data.Message);
          this.setState({loading: false});
        } else {
          this.setState({loading: false});
          // this.showMessage(response.data.errors);
        }
      } catch (e) {
        // var error = (_.get(e, 'response.status', null) ? _.get(e, 'response.data.errors', 'Something went wrong!') : 'Network error!');
        console.log('error : ', e);
        this.setState({
          loading: false,
        });
        // this.showMessage(error);
      }
    });
  }

  render() {
    // return(
    //     <View>

    //     </View>
    // )
    if (this.state.loading) {
      return (
        <SafeAreaView style={[styles.full, styles.backgroundColor]}>
          <StatusBar
            barStyle="default"
            hidden={false}
            backgroundColor="#transparent"
            translucent={true}
          />
          <Header
            onLogo={null}
            label={'Change Password'}
            Agency_logo={this.state.agency_details}
            expanded={this.state.expanded}
          />
          <LoadingIndicator />
        </SafeAreaView>
      );
    } else {
      return (
        <SafeAreaView style={[styles.full, styles.backgroundColor]}>
          <Container>
            <StatusBar
              barStyle="light-content"
              hidden={false}
              backgroundColor="#transparent"
              translucent={true}
            />
            <Header
              onLogo={null}
              label={'Change Password'}
              Agency_logo={this.state.agency_details}
              expanded={this.state.expanded}
            />
            <ScrollView
            // refreshControl={
            //     <RefreshControl
            //         refreshing={this.state.loading}
            //         onRefresh={this._onRefresh}
            //         colors={["#4254C5"]}
            //     />}
            >
              <KeyboardAvoidingView
                behavior={Platform.OS === 'ios' ? 'padding' : null}
                style={[styles.full]}>
                {/* <ScrollView style={{flex:1}}> */}
                <View style={[styles.full, styles.column]}>
                  <View
                    style={[
                      styles.full,
                      styles.column,
                      styles.pd(4),
                      styles.mt(2),
                      {
                        flex: 2,
                        marginBottom: this.state.isFocused
                          ? widthPercentageToDP('6%')
                          : null,
                      },
                    ]}>
                    <View style={[styles.mb(2), styles.normalBorderBottom]}>
                      <Text style={[styles.normalFont, styles.fontSize(2)]}>
                        OLD PASSWORD
                      </Text>
                      <TextInput
                        autoCapitalize="none"
                        style={[
                          styles.pv(1),
                          styles.ph(0),
                          styles.fontSize(2.5),
                          {color: '#828ea5', fontFamily: 'Catamaran-Medium'},
                        ]}
                        placeholder={'Enter Your Old Password'}
                        onFocus={this.handleFocus}
                        onBlur={this.handleBlur}
                        placeholderTextColor="#828ea5"
                        onChangeText={(text) => {
                          this.setState({
                            oldpassword: text,
                          });
                        }}
                      />
                    </View>
                    <View style={[styles.mb(2), styles.normalBorderBottom]}>
                      <Text style={[styles.normalFont, styles.fontSize(2)]}>
                        NEW PASSWORD
                      </Text>
                      <TextInput
                        autoCapitalize="none"
                        // secureTextEntry={true}
                        style={[
                          styles.pv(1),
                          styles.ph(0),
                          styles.boldFont,
                          styles.fontSize(2.5),
                          {color: '#828ea5', fontFamily: 'Catamaran-Medium'},
                        ]}
                        placeholder={'Enter Your New Password'}
                        onFocus={this.handleFocus}
                        onBlur={this.handleBlur}
                        placeholderTextColor="#828ea5"
                        onChangeText={(text) => {
                          this.setState({
                            Newpassword: text,
                          });
                        }}
                      />
                    </View>
                    <View
                      style={[styles.full, styles.column, {borderRadius: 5}]}>
                      <TouchableOpacity
                        style={[styles.mb(2), styles.full]}
                        onPress={async () => {
                          //console.log("internet : ", await this.checkInternet());
                          // if (await this.checkInternet()) {
                          this.ChangePassword();
                          // }
                          // else {
                          // this.showToast("No Internet Connectivity!")
                          // }
                        }}>
                        <LinearGradient
                          colors={[
                            styleConstants.gradientStartColor,
                            styleConstants.gradientEndColor,
                          ]}
                          style={styles.linearGradient}
                          start={{x: 0.0, y: 0.25}}
                          end={{x: 1.2, y: 1.0}}
                          style={[
                            styles.rh(7),
                            styles.center,
                            ,
                            styles.br(0.3),
                            {
                              borderRadius: 5,
                              borderColor: '#fff',
                              shadowColor: '#000',
                              shadowOffset: {
                                width: 0,
                                height: 2,
                              },
                              shadowOpacity: 0.25,
                              shadowRadius: 3.84,
                              elevation: 5,
                            },
                          ]}>
                          {this.state.signloading === true ? (
                            <View
                              style={{
                                flex: 1,
                                flexDirection: 'row',
                                alignItems: 'center',
                                justifyContent: 'center',
                              }}>
                              <View
                                style={{
                                  paddingRight: 10,
                                  backgroundColor: 'transparent',
                                }}>
                                <ActivityIndicator
                                  size={'small'}
                                  color="#FFFFFF"
                                />
                              </View>
                              <View style={{backgroundColor: 'transparent'}}>
                                <Text
                                  style={{
                                    color: '#FFFFFF',
                                    fontFamily: 'Catamaran-Bold',
                                  }}>
                                  Please Wait...
                                </Text>
                              </View>
                            </View>
                          ) : (
                            <Text
                              style={[
                                styles.boldFont,
                                styles.fontSize(2.5),
                                {color: 'white', textAlign: 'center'},
                              ]}>
                              {' '}
                              Change Password
                            </Text>
                          )}
                        </LinearGradient>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
                <View style={[styles.column]}></View>
                {/* </ScrollView> */}
              </KeyboardAvoidingView>
            </ScrollView>
          </Container>
        </SafeAreaView>
      );
    }
  }
}

const style = StyleSheet.create({});

export default ChangePassword;
