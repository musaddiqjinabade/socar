import React from 'react';
import {
  StyleSheet,
  SafeAreaView,
  Image,
  Text,
  View,
  ScrollView,
  ActivityIndicator,
  TextInput,
  Alert,
  TouchableOpacity,
  Platform,
  StatusBar,
} from 'react-native';
import commonStyles from '../styles/Common';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import {withNavigation} from 'react-navigation';
import LoadingIndicator from '../core/components/LoadingIndicator';
import AsyncStorage from '@react-native-async-storage/async-storage';
// import { connect } from "react-redux";
import Globals from '../Global';
import {Toast} from 'native-base';
import Modal from 'react-native-modalbox';
import LinearGradient from 'react-native-linear-gradient';
import StyleConstants from '../styles/StyleConstants';
import Header from '../core/components/Header';
import moment from 'moment';
const axios = require('axios');

class ProfileScreen extends React.Component {
  state = {
    data: '',
    expanded: true,
    loading: false,
    dateArray: [],
    totalCount: 0,
    user_data: '',
    email: '',
  };

  showMessage(message) {
    Toast.show({
      text: message,
      duration: 2000,
      unData: '',
    });
  }

  checkInternet() {}

  GetAddress(value) {
    this.setState({address: value});
  }

  GetCity(value) {
    var data = /^([^0-9]*)$/;
    // if(new RegExp(data){
    // }
    if (data.test(value)) {
      this.setState({city: value});
    }
  }

  GetState(value) {
    var data = /^([^0-9]*)$/;
    // if(new RegExp(data){
    // }
    if (data.test(value)) {
      this.setState({state: value});
    }
  }

  GetZip_code(value) {
    this.setState({zip_code: value});
  }

  GetPhone(value) {
    if (value.length <= 10) {
      this.setState({phone: value});
    }
  }

  GetAbout(ValueHolder) {
    this.setState({about: ValueHolder});
  }

  edit_contact(item) {
    console.log('edit:', item);
    this.setState(
      {
        address: item.Address,
        email: item.Email,
        phone: item.Mobile ? item.Mobile : '---',
        city: item.City,
        zip_code: item.Pincode,
        state: item.State,
      },
      () => {
        this.refs.modal1.open();
      },
    );
  }

  async componentDidMount() {
    await AsyncStorage.getItem('user_token').then((value) => {
      var user_data = JSON.parse(value);
      console.log('userdata::', user_data);
      if (user_data.BuyerId) {
        this.setState({user_data: user_data});
      }
    });
    // this.getdata()
  }

  savecontact(address, phone, city, state, zip_code) {
    this.setState({saveloading: true});
    var onylAlpha = '0123456789`~!@#$%^&*()-_=+{}[]|/><,.:;?';
    var numbers = '`~!@#$%^&*()-_=+{}[]|/><,.:;?';
    var cityInvalid = '';
    var stateInvalid = '';
    var zipInvalid = '';
    for (var i = 0; i < this.state.city.length; i++) {
      if (onylAlpha.indexOf(this.state.city[i]) > -1) {
        cityInvalid = 'Invalid city';
        this.setState({saveloading: false});
      }
    }

    for (var i = 0; i < this.state.state.length; i++) {
      if (onylAlpha.indexOf(this.state.state[i]) > -1) {
        stateInvalid = 'Invalid state';
        this.setState({saveloading: false});
      }
    }

    for (var i = 0; i < this.state.zip_code.length; i++) {
      if (numbers.indexOf(this.state.zip_code[i]) > -1) {
        zipInvalid = 'Invalid zipcode';
        this.setState({saveloading: false});
      }
    }

    if (this.state.email === '') {
      this.showMessage('Please enter email');
      this.setState({saveloading: false});
    } else if (this.state.phone.length !== 10) {
      this.showMessage('Please enter 10 digit phone number');
      this.setState({saveloading: false});
    } else {
      this.UpdateProfile(
        this.state.address,
        this.state.email,
        this.state.phone,
        this.state.city,
        this.state.state,
        this.state.zip_code,
      );
    }
  }

  UpdateProfile(address, email, phone, city, state, zip_code) {
    this.setState({loading: true}, async () => {
      this.setState({loading: true});
      var url =
        Globals.domain +
        'Updateprofile?buyerid=' +
        this.state.user_data.BuyerId +
        '&fname=' +
        this.state.user_data.FirstName +
        '&lname=' +
        this.state.user_data.LastName +
        '&address=' +
        address +
        '&city=' +
        city +
        '&country=india&state=' +
        state +
        '&pin=' +
        zip_code +
        '&mobile=' +
        phone;
      console.log('sessions', url);
      const headers = {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      };
      try {
        var response = await axios.post(url, null, {
          headers: headers,
        });
        console.log('data : ', response);

        if (response.status == 200) {
          this.showMessage(response.data.Message);
          this.setState({loading: false});
        } else {
          this.setState({loading: false});
          // this.showMessage(response.data.errors);
        }
      } catch (e) {
        // var error = (_.get(e, 'response.status', null) ? _.get(e, 'response.data.errors', 'Something went wrong!') : 'Network error!');
        console.log('error : ', e);
        this.setState({
          loading: false,
        });
        // this.showMessage(error);
      }
    });
  }

  _onRefresh = () => {
    this.setState({loading: true}, () => {
      // this.componentWillMount();
    });
  };

  async logout() {
    Alert.alert(
      'Log out',
      'You will be returned to the login screen.',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {
          text: 'Log out',
          onPress: async () => {
            this.props.navigation.navigate('LoginScreen');

            // await AsyncStorage.removeItem('agency_code', null);
            // await AsyncStorage.removeItem('USER_DATA', null);
            await AsyncStorage.removeItem('user_token');
          },
        },
      ],
      {cancelable: false},
    );
  }

  render() {
    // return(
    //     <View>

    //     </View>
    // )
    if (this.state.loading) {
      return (
        <SafeAreaView style={[commonStyles.full, commonStyles.backgroundColor]}>
          <StatusBar
            barStyle="default"
            hidden={false}
            backgroundColor="#transparent"
            translucent={true}
          />
          <Header
            onLogo={null}
            label={'Manage Profile'}
            Agency_logo={this.state.agency_details}
            expanded={this.state.expanded}
          />
          <LoadingIndicator />
        </SafeAreaView>
      );
    } else {
      const userData = null;
      return (
        <SafeAreaView style={[commonStyles.full, commonStyles.backgroundColor]}>
          <StatusBar
            barStyle="light-content"
            hidden={false}
            backgroundColor="#transparent"
            translucent={true}
          />
          <Header
            onLogo={null}
            label={'Manage Profile'}
            Agency_logo={this.state.agency_details}
            expanded={this.state.expanded}
          />
          <ScrollView
          // refreshControl={
          //     <RefreshControl
          //         refreshing={this.state.loading}
          //         onRefresh={this._onRefresh}
          //         colors={["#4254C5"]}
          //     />}
          >
            <View
              style={[
                commonStyles.margin,
                commonStyles.column,
                commonStyles.full,
              ]}>
              <View style={[commonStyles.center]}>
                <TouchableOpacity
                  style={{
                    justifyContent: 'flex-start',
                    alignSelf: 'flex-end',
                    flexDirection: 'row',
                  }}
                  onPress={() => {
                    this.logout();
                  }}>
                  <Image
                    style={[styles.logoutImage]}
                    resizeMode={'contain'}
                    resizeMethod={'resize'} // <-------  this helped a lot as OP said
                    // progressiveRenderingEnabled={true}
                    source={require('../assets/images/logout_new.png')}
                  />
                  <Text
                    style={[
                      commonStyles.headerTextBoldFont,
                      {
                        justifyContent: 'center',
                        alignSelf: 'center',
                        marginBottom:
                          Platform.OS == 'ios' ? hp('1%') : hp('2%'),
                        marginLeft: wp('2%'),
                      },
                    ]}>
                    {'Log out'}
                  </Text>
                </TouchableOpacity>
                {userData && userData.avatar_url ? (
                  <View>
                    <Image
                      style={[styles.clientImage]}
                      resizeMode={'cover'}
                      resizeMethod={'resize'} // <-------  this helped a lot as OP said
                      // progressiveRenderingEnabled={true}
                      source={{uri: userData.avatar_url}}
                    />
                  </View>
                ) : (
                  <View
                    style={[
                      styles.clientImage,
                      commonStyles.center,
                      commonStyles.mt(2),
                      {backgroundColor: '#fff'},
                    ]}>
                    <Text
                      style={[
                        commonStyles.boldFont,
                        commonStyles.fs(10),
                        {color: StyleConstants.gradientStartColor},
                      ]}>
                      {this.state.user_data
                        ? this.state.user_data.FirstName.charAt(0).toUpperCase()
                        : null}
                    </Text>
                  </View>
                )}
                <Text style={[commonStyles.headerTextBoldFont]}>
                  {this.state.user_data
                    ? this.state.user_data.FirstName.toUpperCase() +
                      ' ' +
                      this.state.user_data.LastName.toUpperCase()
                    : '--'}
                </Text>
                {/* <Text style={{ marginTop: -5, fontSize: hp('2%'), color: '#828EA5', fontFamily: 'Catamaran-Regular', }}>Caregiver</Text> */}
              </View>

              <View style={[{flexDirection: 'row', marginTop: wp('4%')}]}>
                <View
                  style={{
                    paddingRight: wp('3%'),
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'flex-start',
                  }}>
                  <Text style={[styles.itemHeader]}>Contact Details</Text>
                </View>
                <TouchableOpacity
                  onPress={() => this.edit_contact(this.state.user_data)}
                  style={[
                    commonStyles.center,
                    {alignItems: 'flex-end', marginRight: wp('3%')},
                  ]}>
                  <Image
                    style={styles.editImage}
                    color="#4254C5"
                    source={require('../assets/images/edit.png')}
                  />
                </TouchableOpacity>
              </View>

              <View style={[commonStyles.row, {marginTop: wp('1%')}]}>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'flex-start',
                    alignSelf: 'center',
                  }}>
                  <Image
                    style={styles.otherImages}
                    source={require('../assets/images/location.png')}
                  />
                </View>
                <TouchableOpacity
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginLeft: wp('3%'),
                  }}
                  onPress={() => {
                    this.openMaps(
                      userData.full_address,
                      userData.city,
                      userData.state,
                      userData.zip_code,
                    );
                  }}>
                  <Text
                    style={{
                      fontFamily: 'Catamaran-Regular',
                      color: '#41A2EB',
                      textDecorationLine: 'underline',
                      fontSize: hp('2%'),
                    }}>
                    {this.state.user_data.Address
                      ? this.state.user_data.Address + ' ,'
                      : '--'}
                    {this.state.user_data.City
                      ? this.state.user_data.City + ' ,'
                      : '--'}
                    {this.state.user_data.State
                      ? this.state.user_data.State + ' ,'
                      : '--'}
                    {this.state.user_data.Pincode
                      ? this.state.user_data.Pincode + '.'
                      : '--'}
                  </Text>
                </TouchableOpacity>
              </View>

              <View style={[commonStyles.row]}>
                <View style={{flexDirection: 'row', alignItems: 'flex-end'}}>
                  <Image
                    style={styles.otherImages}
                    source={require('../assets/images/email.png')}
                  />
                </View>

                <TouchableOpacity
                  onPress={() =>
                    Linking.openURL(
                      'mailto:' +
                        this.props.data.user[
                          Object.keys(this.props.data.user)[0]
                        ].email,
                    )
                  }
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginLeft: wp('3%'),
                  }}>
                  <Text
                    style={{
                      fontFamily: 'Catamaran-Regular',
                      color: '#41A2EB',
                      textDecorationLine: 'underline',
                      fontSize: hp('2%'),
                    }}>
                    {this.state.user_data.Email
                      ? this.state.user_data.Email
                      : '---'}
                  </Text>
                </TouchableOpacity>
              </View>

              <View style={[commonStyles.row]}>
                <View style={{flexDirection: 'row', alignItems: 'flex-end'}}>
                  <Image
                    style={styles.otherImages}
                    source={require('../assets/images/phone-call.png')}
                  />
                </View>
                {/* <TouchableOpacity onPress={() => Linking.openURL(`tel:${this.state.data.phone}`)}> */}
                <TouchableOpacity
                  onPress={() => Linking.openURL(`tel:${userData.phone}`)}
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginLeft: wp('3%'),
                  }}>
                  <Text
                    style={{
                      fontFamily: 'Catamaran-Regular',
                      color: this.state.user_data.Mobile
                        ? '#41A2EB'
                        : '#828EA5',
                      textDecorationLine: this.state.user_data.Mobile
                        ? 'underline'
                        : null,
                      fontSize: hp('2%'),
                    }}>
                    {this.state.user_data.Mobile
                      ? this.state.user_data.Mobile
                      : '---'}
                  </Text>
                </TouchableOpacity>
              </View>

              <View style={[commonStyles.row]}>
                <View style={{flexDirection: 'row', alignItems: 'flex-end'}}>
                  {/* <Image style={styles.otherImages}
                                        source={require('../../assets/images/calendar.png')} /> */}
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginLeft: wp('3%'),
                  }}>
                  {/* <Text style={{ fontFamily: 'Catamaran-Regular', color: '#828EA5', fontSize: hp('2%') }}>Joined Date : {moment(userData.joined_date).format("MMM DD, YYYY")}</Text> */}
                </View>
              </View>
            </View>
          </ScrollView>

          <Modal
            style={[styles.modal, styles.modal2]}
            position={'center'}
            ref={'modal1'}
            swipeArea={20}
            backdropPressToClose={true}>
            <ScrollView keyboardShouldPersistTaps={true} style={{flex: 1}}>
              <View
                style={{
                  flex: 1,
                  width: wp('85%'),
                  justifyContent: 'center',
                  alignContent: 'center',
                  backgroundColor: '#fff',
                  borderRadius: 4,
                  flexDirection: 'column',
                  paddingHorizontal: wp('1%'),
                  paddingVertical: wp('1%'),
                }}>
                <TouchableOpacity
                  style={[
                    commonStyles.mb(0.1),
                    {
                      justifyContent: 'space-between',
                      flexDirection: 'row',
                      alignItems: 'flex-end',
                      marginRight: wp('3%'),
                    },
                  ]}
                  onPress={() => {
                    this.refs.modal1.close();
                  }}>
                  <View style={{marginLeft: wp('6%')}}></View>
                  <Text
                    style={[
                      commonStyles.boldFont,
                      commonStyles.fontSize(3),
                      {
                        justifyContent: 'center',
                        alignSelf: 'center',
                        color: '#3B5793',
                        marginLeft: wp('2%'),
                      },
                    ]}>
                    Edit Contact Details
                  </Text>
                  <Image
                    style={[
                      {
                        height: hp('5%'),
                        width: wp('5%'),
                        resizeMode: 'contain',
                        alignSelf: 'flex-end',
                      },
                    ]}
                    source={require('../assets/images/error.png')}
                  />
                </TouchableOpacity>
                <View
                  style={[
                    commonStyles.mb(0.1),
                    {margin: wp('4%'), marginTop: wp('3.1%')},
                  ]}>
                  <Text
                    style={[
                      commonStyles.boldFont,
                      commonStyles.fs(1.9),
                      {
                        color: '#3B5793',
                        fontFamily: 'Catamaran-Medium',
                        marginLeft: wp('1%'),
                      },
                    ]}>
                    Address
                  </Text>
                  <TextInput
                    style={[
                      commonStyles.boldFont,
                      commonStyles.fs(2.3),
                      {
                        color: '#828ea5',
                        fontFamily: 'Catamaran-Medium',
                        borderBottomColor: '#000',
                        borderBottomWidth: Platform.OS === 'ios' ? 1 : null,
                      },
                    ]}
                    underlineColorAndroid={'#828ea5'}
                    onFocus={this.handleFocustitle}
                    // autoFocus={true}
                    onBlur={this.handleBlurtitle}
                    multiline
                    keyboardType={'email-address'}
                    numberOfLines={1}
                    autoCapitalize="characters"
                    placeholder={'Enter Address'}
                    placeholderTextColor="#828ea5"
                    onChangeText={(ValueHolder) => this.GetAddress(ValueHolder)}
                    value={this.state.address}
                  />
                </View>
                <View
                  style={[
                    commonStyles.mb(0.1),
                    {margin: wp('4%'), marginTop: wp('0.1%')},
                  ]}>
                  <Text
                    style={[
                      commonStyles.boldFont,
                      commonStyles.fs(1.9),
                      {
                        color: '#3B5793',
                        fontFamily: 'Catamaran-Medium',
                        marginLeft: wp('1%'),
                      },
                    ]}>
                    City
                  </Text>
                  <TextInput
                    style={[
                      commonStyles.boldFont,
                      commonStyles.fs(2.3),
                      {
                        color: '#828ea5',
                        fontFamily: 'Catamaran-Medium',
                        borderBottomColor: '#000',
                        borderBottomWidth: Platform.OS === 'ios' ? 1 : null,
                      },
                    ]}
                    underlineColorAndroid={'#828ea5'}
                    onFocus={this.handleFocustitle}
                    onBlur={this.handleBlurtitle}
                    multiline
                    numberOfLines={1}
                    placeholder={'Enter Email'}
                    placeholderTextColor="#828ea5"
                    onChangeText={(ValueHolder) =>
                      this.setState({email: ValueHolder})
                    }
                    value={this.state.email}
                  />
                </View>
                <View
                  style={[
                    commonStyles.mb(0.1),
                    {margin: wp('4%'), marginTop: wp('0.1%')},
                  ]}>
                  <Text
                    style={[
                      commonStyles.boldFont,
                      commonStyles.fs(1.9),
                      {
                        color: '#3B5793',
                        fontFamily: 'Catamaran-Medium',
                        marginLeft: wp('1%'),
                      },
                    ]}>
                    City
                  </Text>
                  <TextInput
                    style={[
                      commonStyles.boldFont,
                      commonStyles.fs(2.3),
                      {
                        color: '#828ea5',
                        fontFamily: 'Catamaran-Medium',
                        borderBottomColor: '#000',
                        borderBottomWidth: Platform.OS === 'ios' ? 1 : null,
                      },
                    ]}
                    underlineColorAndroid={'#828ea5'}
                    onFocus={this.handleFocustitle}
                    onBlur={this.handleBlurtitle}
                    multiline
                    numberOfLines={1}
                    placeholder={'Enter City'}
                    placeholderTextColor="#828ea5"
                    onChangeText={(ValueHolder) => this.GetCity(ValueHolder)}
                    value={this.state.city}
                  />
                </View>
                <View
                  style={[
                    commonStyles.mb(0.1),
                    {margin: wp('4%'), marginTop: wp('0.1%')},
                  ]}>
                  <Text
                    style={[
                      commonStyles.boldFont,
                      commonStyles.fs(1.9),
                      {
                        color: '#3B5793',
                        fontFamily: 'Catamaran-Medium',
                        marginLeft: wp('1%'),
                      },
                    ]}>
                    State
                  </Text>
                  <TextInput
                    style={[
                      commonStyles.boldFont,
                      commonStyles.fs(2.3),
                      {
                        color: '#828ea5',
                        fontFamily: 'Catamaran-Medium',
                        borderBottomColor: '#000',
                        borderBottomWidth: Platform.OS === 'ios' ? 1 : null,
                      },
                    ]}
                    underlineColorAndroid={'#828ea5'}
                    onFocus={this.handleFocustitle}
                    onBlur={this.handleBlurtitle}
                    multiline
                    numberOfLines={1}
                    placeholder={'Enter State'}
                    placeholderTextColor="#828ea5"
                    onChangeText={(ValueHolder) => this.GetState(ValueHolder)}
                    value={this.state.state}
                  />
                </View>
                <View
                  style={[
                    commonStyles.mb(0.1),
                    {margin: wp('4%'), marginTop: wp('0.1%')},
                  ]}>
                  <Text
                    style={[
                      commonStyles.boldFont,
                      commonStyles.fs(1.9),
                      {
                        color: '#3B5793',
                        fontFamily: 'Catamaran-Medium',
                        marginLeft: wp('1%'),
                      },
                    ]}>
                    Zip Code
                  </Text>
                  <TextInput
                    style={[
                      commonStyles.boldFont,
                      commonStyles.fs(2.3),
                      {
                        color: '#828ea5',
                        fontFamily: 'Catamaran-Medium',
                        borderBottomColor: '#000',
                        borderBottomWidth: Platform.OS === 'ios' ? 1 : null,
                      },
                    ]}
                    underlineColorAndroid={'#828ea5'}
                    onFocus={this.handleFocustitle}
                    onBlur={this.handleBlurtitle}
                    multiline
                    keyboardType={'phone-pad'}
                    numberOfLines={1}
                    placeholder={'Enter Zip code'}
                    placeholderTextColor="#828ea5"
                    onChangeText={(ValueHolder) =>
                      this.GetZip_code(ValueHolder)
                    }
                    value={this.state.zip_code}
                  />
                </View>
                <View
                  style={[
                    commonStyles.mb(0.1),
                    {margin: wp('4%'), marginTop: wp('0.1%')},
                  ]}>
                  <Text
                    style={[
                      commonStyles.boldFont,
                      commonStyles.fs(1.9),
                      {
                        color: '#3B5793',
                        fontFamily: 'Catamaran-Medium',
                        marginLeft: wp('1%'),
                      },
                    ]}>
                    Phone
                  </Text>
                  <TextInput
                    style={[
                      commonStyles.boldFont,
                      commonStyles.fs(2.3),
                      {
                        color: '#828ea5',
                        fontFamily: 'Catamaran-Medium',
                        borderBottomColor: '#000',
                        borderBottomWidth: Platform.OS === 'ios' ? 1 : null,
                      },
                    ]}
                    underlineColorAndroid={'#828ea5'}
                    onFocus={this.handleFocustitle}
                    onBlur={this.handleBlurtitle}
                    multiline
                    keyboardType={'phone-pad'}
                    numberOfLines={1}
                    placeholder={'Enter Phone'}
                    placeholderTextColor="#828ea5"
                    onChangeText={(ValueHolder) => this.GetPhone(ValueHolder)}
                    value={this.state.phone}
                  />
                </View>
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignSelf: 'center',
                    flexDirection: 'row',
                    margin: hp('2%'),
                  }}>
                  <LinearGradient
                    colors={[
                      StyleConstants.gradientStartColor,
                      StyleConstants.gradientEndColor,
                    ]}
                    // style={styles.linearGradient}
                    start={{x: 0.0, y: 0.25}}
                    end={{x: 1.2, y: 1.0}}
                    style={[
                      commonStyles.rh(7),
                      commonStyles.center,
                      commonStyles.br(0.3),
                      {
                        width: this.state.reimloading ? wp('35%') : wp('25%'),
                        height: wp('13%'),
                        borderRadius: 5,
                        borderColor: '#fff',
                        shadowColor: '#000',
                        shadowOffset: {
                          width: 0,
                          height: 2,
                        },
                        shadowOpacity: 0.25,
                        shadowRadius: 3.84,
                        elevation: 5,
                      },
                    ]}>
                    <TouchableOpacity
                      style={{
                        justifyContent: 'center',
                        alignSelf: 'center',
                        borderRadius: 8,
                      }}
                      onPress={async () => {
                        this.savecontact(
                          this.state.address,
                          this.state.email,
                          this.state.phone,
                          this.state.city,
                          this.state.state,
                          this.state.zip_code,
                        );
                      }}>
                      {this.state.saveloading === true ? (
                        <View
                          style={{
                            flex: 1,
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'center',
                          }}>
                          <View
                            style={{
                              paddingRight: 10,
                              backgroundColor: 'transparent',
                            }}>
                            <ActivityIndicator size={'small'} color="#FFFFFF" />
                          </View>
                          <View style={{backgroundColor: 'transparent'}}>
                            <Text
                              style={{
                                color: '#FFFFFF',
                                fontFamily: 'Catamaran-Bold',
                              }}>
                              Please Wait...
                            </Text>
                          </View>
                        </View>
                      ) : (
                        <Text
                          style={[
                            commonStyles.boldFont,
                            {
                              color: '#ffffff',
                              justifyContent: 'center',
                              alignSelf: 'center',
                              marginHorizontal: wp('2%'),
                            },
                          ]}>
                          Update
                        </Text>
                      )}
                    </TouchableOpacity>
                  </LinearGradient>
                </View>
              </View>
            </ScrollView>
          </Modal>
        </SafeAreaView>
      );
    }
  }
}

const styles = StyleSheet.create({
  logoutImage: {
    width: wp('7%'),
    height: hp('3%'),
    justifyContent: 'center',
    alignSelf: 'center',
  },
  editImage: {
    width: wp('4%'),
    height: hp('4%'),
    resizeMode: 'contain',
  },
  otherImages: {
    width: wp('3.5%'),
    height: hp('3.5%'),
    resizeMode: 'contain',
  },
  clientImage: {
    width: wp('18%'),
    height: hp('18%'),
    aspectRatio: 1,
    borderColor: '#fff',
    backgroundColor: '#CFCFCF',
    borderBottomLeftRadius: 8,
    borderBottomRightRadius: 8,
    borderTopRightRadius: 8,
    borderTopLeftRadius: 8,
  },
  itemHeader: {
    fontFamily: 'Catamaran-Bold',
    color: '#293D68',
    fontSize: hp('2.2%'),
  },
  smallImage: {
    justifyContent: 'flex-end',
    alignContent: 'flex-end',
    margin: hp('1%'),
    width: wp('8%'),
    height: hp('8%'),
    aspectRatio: 1,
  },
  modal: {
    justifyContent: 'center',
    alignItems: 'center',
    // position: "absolute",
    backgroundColor: 'rgba(0, 0, 0, 0)',
  },
  modal2: {
    // maxHeight: 500,
    // minHeight: 100
    // height: wp('108%')
    flex: 1,
  },
  modal3: {
    justifyContent: 'center',
    alignItems: 'center',
    // position: "absolute",
    backgroundColor: 'rgba(0, 0, 0, 0)',
  },
  modal4: {
    maxHeight: 500,
    minHeight: 100,
    height: wp('108%'),
    flex: 1,
  },
  calendarImage: {
    width: wp('5%'),
    height: hp('5%'),
    resizeMode: 'contain',
  },
  rectangleShapeFirst: {
    padding: 5,
    borderRadius: 5,
    marginBottom: 10,
    width: wp('35%'),
    height: hp('12%'),
    // borderWidth:1,
    // borderColor:'#e6e6e6',
    backgroundColor: '#FFFFFF',
  },
  rectangleShapeOthers: {
    padding: 5,
    borderRadius: 5,
    marginLeft: wp('5%'),
    width: wp('35%'),
    height: hp('12%'),
    backgroundColor: '#FFFFFF',
  },
  rectangleShapeLast: {
    padding: 5,
    borderRadius: 5,
    marginLeft: wp('5%'),
    width: wp('35%'),
    marginBottom: 10,
    height: hp('12%'),
    backgroundColor: '#FFFFFF',
  },
  circle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'center',
    alignSelf: 'flex-end',
    width: 17,
    height: 17,
    borderRadius: 50,
    backgroundColor: '#65CA8F',
  },
  rectangleShapeDashboardMenu: {
    flex: 1,
    borderRadius: 5,
    width: wp('42%'),
    height: hp('16%'),
    // borderWidth:1,
    // borderColor:'#e6e6e6',
    backgroundColor: '#FFFFFF',
    justifyContent: 'center',
  },
  rectangleShapeDashboardMenuSecond: {
    flex: 1,
    marginLeft: wp('5%'),
    borderRadius: 5,
    paddingBottom: widthPercentageToDP('2%'),
    width: wp('42%'),
    height: hp('16%'),
    justifyContent: 'center',
    backgroundColor: '#FFFFFF',
    // borderWidth:1,
    // borderColor:'#f2f2f2',
  },
  smallRectangleShapeWhite: {
    marginTop: 10,
    marginRight: 10,
    paddingTop: 2,
    paddingBottom: 2,
    paddingRight: 7,
    paddingLeft: 7,
    alignSelf: 'flex-end',
    borderRadius: 5,
    backgroundColor: '#ffffff',
  },
  smallRectangleShapeRed: {
    marginTop: wp('-2%'),
    marginRight: 10,
    paddingRight: 7,
    paddingLeft: 7,
    alignSelf: 'flex-end',
    borderRadius: 5,
    backgroundColor: '#EA5C5B',
  },
  RectangleShapeRed: {
    marginTop: hp('0.3%'),
    marginRight: 10,
    paddingRight: 7,
    paddingLeft: 7,
    alignSelf: 'flex-end',
    borderRadius: 5,
    backgroundColor: '#EA5C5B',
  },
  MenuImage: {
    width: wp('6%'),
    height: hp('6%'),
    resizeMode: 'contain',
  },
  modal: {
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    backgroundColor: 'rgba(0, 0, 0, 0)',
  },
  modal1: {
    maxHeight: 315,
    minHeight: 80,
  },
  blankCircle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'center',
    alignSelf: 'flex-end',
    width: 17,
    height: 17,
    borderRadius: 50,
    backgroundColor: '#ffffff',
  },
});

export default withNavigation(ProfileScreen);
