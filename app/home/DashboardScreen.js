import React, {useEffect, useRef} from 'react';
import {
  StyleSheet,
  SafeAreaView,
  Image,
  Text,
  View,
  TouchableOpacity,
  StatusBar,
  Dimensions,
} from 'react-native';
import commonStyles from '../styles/Common';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import LoadingIndicator from '../core/components/LoadingIndicator';
import {Toast} from 'native-base';
const axios = require('axios');
import MapView, {PROVIDER_GOOGLE, Marker, Callout} from 'react-native-maps';
import firebase from '@react-native-firebase/app';
import {TextInput} from 'react-native-gesture-handler';
import Geolocation from '@react-native-community/geolocation';
import Geocoder from 'react-native-geocoding';
// import firebase from '../../config'
import database from '@react-native-firebase/database';
import Header from '../core/components/Header';

class DashboardScreen extends React.Component {
  state = {
    data: '',
    expanded: true,
    loading: false,
    dateArray: [],
    totalCount: 0,
    clientData: [],
    initialRegion: {
      latitude: 3.149771,
      longitude: 101.655449,
      latitudeDelta: 0.00323,
      longitudeDelta: 0.00323,
    },
    marginBottom: 1,
    currentAddress: '',
    markers: [
      {
        title: 'hello',
        coordinates: {
          latitude: 3.148771,
          longitude: 101.655449,
        },
        car_img: 'https://i.ibb.co/7QW9YVF/wp1972478-honda-city-wallpapers.jpg',
      },
      {
        title: 'hello',
        coordinates: {
          latitude: 3.349771,
          longitude: 101.755449,
        },
        car_img: 'https://i.ibb.co/7QW9YVF/wp1972478-honda-city-wallpapers.jpg',
      },
      {
        title: 'hello',
        coordinates: {
          latitude: 3.359771,
          longitude: 101.555449,
        },
        car_img: 'https://i.ibb.co/7QW9YVF/wp1972478-honda-city-wallpapers.jpg',
      },
      {
        title: 'hello',
        coordinates: {
          latitude: 3.369771,
          longitude: 101.655449,
        },
        car_img: 'https://i.ibb.co/7QW9YVF/wp1972478-honda-city-wallpapers.jpg',
      },
      {
        title: 'hello',
        coordinates: {
          latitude: 3.379771,
          longitude: 101.755449,
        },
        car_img: 'https://i.ibb.co/7QW9YVF/wp1972478-honda-city-wallpapers.jpg',
      },
    ],
  };

  showMessage(message) {
    Toast.show({
      text: message,
      duration: 2000,
      unData: '',
    });
  }

  checkInternet() {}

  fetchdetails() {
    database()
      .ref('/users')
      .once('value')
      .then((snapshot) => {
        console.log('snapshot', snapshot);
        var data = [];

        snapshot.forEach((item) => {
          data.push(item);
        });
        // this.setState({markers:data})
      });
  }

  async componentDidMount() {
    // console.log('firebase', firebase.app())
    this.fetchdetails();

    // this.getdata() 0.004596970975399017
    Geolocation.getCurrentPosition((pos) => {
      console.log('pos', pos);
      if (pos.coords.latitude) {
        var region = {};
        region.latitude = pos.coords.latitude;
        region.longitude = pos.coords.longitude;
        (region.latitudeDelta = 0.007047028565072111),
          (region.longitudeDelta = 0.004596970975399017);
        // this.setState({
        //     initialRegion: region
        // })
        console.log('address 1:', region.latitude, region.longitude);

        Geocoder.init('AIzaSyB3omSL51fHxuyzJT9OBmJdWzs0oxo4qk4');
        Geocoder.from(pos.coords.latitude, pos.coords.longitude)
          .then((json) => {
            console.log('address jsonn:', json);
            alert(json);
          })
          .catch((error) => console.log(error));
      }
    });
    // this.handlerUserlocation()
    // Geolocation.getCurrentPosition(info => console.log(info))
  }

  handlerUserlocation = () => {
    Geolocation.getCurrentPosition(
      (pos) => {
        // this.map.animateToRegion({
        //     ...this.state.initialRegion,
        //     latitude:pos.coords.latitude,
        //     longitude:pos.coords.longitude

        // })

        this.setState({
          latitude: pos.coords.latitude,
          longitude: pos.coords.longitude,
          latitudeDelta: 0.007047028565072111,
          longitudeDelta:
            Dimensions.get('window').width / Dimensions.get('window').height,
        });
      },
      (err) => {
        console.log(err);
      },
      {
        enableHighAccuracy: true,
        timeout: 3600000,
        maximumAge: 3600000,
      },
    );
  };

  _onRefresh = () => {
    this.setState({loading: true}, () => {
      this.getdata();
    });
  };

  onChangeValue = (initialRegion) => {
    // this.setState({
    //     initialRegion: initialRegion
    // })
    console.log('region', initialRegion);
    // this.getAddress()
    if (initialRegion.latitude) {
      Geocoder.init('AIzaSyAk5n3OCL-9S71awG9uEeqxrsNbroj3EEk');
      Geocoder.from(initialRegion.latitude, initialRegion.longitude)
        .then((json) => {
          console.log('address jsonn:', json);
          this.setState({currentAddress: json.plus_code.compound_code});
        })
        .catch((error) => console.log('error in onchamge', error));
    }
  };

  getAddress = async (lat, lng) => {
    await Geolocation.fallbackToGoogle(
      'AIzaSyB3omSL51fHxuyzJT9OBmJdWzs0oxo4qk4',
    );
    try {
      let res = await Geolocation.geocodePosition({lat, lng});
      let address = res[0].forwardAddress;
      console.log('address:', address);
      this.setState({
        currentAddress: address,
      });
      {
        this.mark.showCallout();
      }
    } catch (err) {
      alert(err);
    }
  };

  addcars() {
    console.log('working');
    const defaultDoc = {
      coordinates: {
        latitude: 16.6348078,
        longitude: 74.8468751,
      },
      car_address: 'Kuala Lumpur',
      car_model: 'Honda Prime',
      car_status: true,
      car_img: 'https://i.ibb.co/7QW9YVF/wp1972478-honda-city-wallpapers.jpg',
    };
    database()
      .ref('/users/Cars')
      .push(defaultDoc)
      .then((snapshot) => {
        console.log('User data before: ', snapshot);
      });
  }

  render() {
    if (this.state.loading) {
      return (
        <SafeAreaView style={[commonStyles.full, commonStyles.backgroundColor]}>
          <StatusBar
            barStyle="default"
            hidden={false}
            backgroundColor="transparent"
            translucent={true}
          />
          <Header
            onLogo={null}
            label={'Dashboard'}
            Agency_logo={this.state.agency_details}
            expanded={this.state.expanded}
          />
          <LoadingIndicator />
        </SafeAreaView>
      );
    } else {
      return (
        <SafeAreaView style={[commonStyles.full, commonStyles.backgroundColor]}>
          <StatusBar
            barStyle="light-content"
            hidden={false}
            backgroundColor="transparent"
            translucent={true}
          />
          <Header
            onLogo={null}
            label={'Dashboard'}
            Agency_logo={this.state.agency_details}
            expanded={this.state.expanded}
          />
          <View>
            <MapView
              provider={PROVIDER_GOOGLE} // remove if not using Google Maps
              style={styles.map}
              region={this.state.initialRegion}
              onRegionChangeComplete={this.onChangeValue}
              ref={(ref) => (this.map = ref)}
              showsUserLocation={true}
              showsMyLocationButton={true}
              onMapReady={() => this.setState({marginBottom: 10})}>
              <Marker
                coordinate={this.state.initialRegion}
                image={require('../assets/images/map_marker.png')}
                title={'Your at Your Location'}
                description="This is the test description">
                <Callout tooltip>
                  <View>
                    <View style={styles.bubble}>
                      <Text style={styles.name}>
                        {this.state.currentAddress}
                      </Text>
                      <Image
                        style={styles.image}
                        source={require('../assets/images/map_marker.png')}
                      />
                    </View>
                    <View style={styles.arrowBorder} />
                    <View style={styles.arrow} />
                  </View>
                </Callout>
              </Marker>
              {this.state.markers.map((marker, index) => (
                <Marker
                  key={index}
                  coordinate={marker.coordinates}
                  image={require('../assets/images/map_marker.png')}
                  title={marker.title}
                  description="This is the test description">
                  <Callout tooltip>
                    <View style={{borderWidth: 1}}>
                      <View style={styles.bubble}>
                        <Text style={styles.name}>{marker.title}</Text>
                        <Image
                          style={styles.image}
                          source={{
                            uri:
                              'https://i.ibb.co/7QW9YVF/wp1972478-honda-city-wallpapers.jpg',
                          }}
                        />
                      </View>
                      <View style={styles.arrowBorder} />
                      <View style={styles.arrow} />
                    </View>
                  </Callout>
                </Marker>
              ))}
            </MapView>
            <View style={styles.searchBox}>
              <TextInput
                placeholder="Please search SOCAR zone or select zone"
                placeholderTextColor="#000"
                autoCapitalize="none"
                value={this.state.currentAddress}
                style={{flex: 1, padding: 0}}
              />
              <Image
                style={styles.searchimage}
                source={require('../assets/images/search-gray.png')}
              />
            </View>
          </View>

          <TouchableOpacity
            onPress={() => {
              this.addcars();
            }}>
            <Text>Add Cars</Text>
          </TouchableOpacity>
        </SafeAreaView>
      );
    }
  }
}

const styles = StyleSheet.create({
  searchBox: {
    position: 'absolute',
    marginTop: 40,
    flexDirection: 'row',
    backgroundColor: 'white',
    width: '90%',
    alignSelf: 'center',
    borderRadius: 5,
    padding: 10,
    shadowColor: '#ccc',
    shadowOffset: {width: 0, height: 3},
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 10,
  },
  calendarImage: {
    width: wp('5%'),
    height: hp('5%'),
    resizeMode: 'contain',
  },
  rectangleShapeFirst: {
    padding: 5,
    borderRadius: 5,
    marginBottom: 10,
    width: wp('35%'),
    height: hp('12%'),
    // borderWidth:1,
    // borderColor:'#e6e6e6',
    backgroundColor: '#FFFFFF',
  },
  rectangleShapeOthers: {
    padding: 5,
    borderRadius: 5,
    marginLeft: wp('5%'),
    width: wp('35%'),
    height: hp('12%'),
    backgroundColor: '#FFFFFF',
  },
  rectangleShapeLast: {
    padding: 5,
    borderRadius: 5,
    marginLeft: wp('5%'),
    width: wp('35%'),
    marginBottom: 10,
    height: hp('12%'),
    backgroundColor: '#FFFFFF',
  },
  circle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'center',
    alignSelf: 'flex-end',
    width: 17,
    height: 17,
    borderRadius: 50,
    backgroundColor: '#65CA8F',
  },
  rectangleShapeDashboardMenu: {
    flex: 1,
    borderRadius: 5,
    width: wp('42%'),
    height: hp('16%'),
    // borderWidth:1,
    // borderColor:'#e6e6e6',
    backgroundColor: '#FFFFFF',
    justifyContent: 'center',
  },
  rectangleShapeDashboardMenuSecond: {
    flex: 1,
    marginLeft: wp('5%'),
    borderRadius: 5,
    paddingBottom: wp('2%'),
    width: wp('42%'),
    height: hp('16%'),
    justifyContent: 'center',
    backgroundColor: '#FFFFFF',
    // borderWidth:1,
    // borderColor:'#f2f2f2',
  },
  smallRectangleShapeWhite: {
    marginTop: 10,
    marginRight: 10,
    paddingTop: 2,
    paddingBottom: 2,
    paddingRight: 7,
    paddingLeft: 7,
    alignSelf: 'flex-end',
    borderRadius: 5,
    backgroundColor: '#ffffff',
  },
  smallRectangleShapeRed: {
    marginTop: wp('-2%'),
    marginRight: 10,
    paddingRight: 7,
    paddingLeft: 7,
    alignSelf: 'flex-end',
    borderRadius: 5,
    backgroundColor: '#EA5C5B',
  },
  RectangleShapeRed: {
    marginTop: hp('0.3%'),
    marginRight: 10,
    paddingRight: 7,
    paddingLeft: 7,
    alignSelf: 'flex-end',
    borderRadius: 5,
    backgroundColor: '#EA5C5B',
  },
  MenuImage: {
    width: wp('6%'),
    height: hp('6%'),
    resizeMode: 'contain',
  },
  modal: {
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    backgroundColor: 'rgba(0, 0, 0, 0)',
  },
  modal1: {
    maxHeight: 315,
    minHeight: 80,
  },
  blankCircle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'center',
    alignSelf: 'flex-end',
    width: 17,
    height: 17,
    borderRadius: 50,
    backgroundColor: '#ffffff',
  },
  line: {
    borderWidth: 0.25,
    borderColor: '#828EA5',
    marginLeft: wp('1%'),
    marginRight: wp('1%'),
    marginBottom: wp('1.5%'),
    marginTop: wp('2%'),
  },
  itemRectangleShape: {
    marginTop: wp('3%'),
    borderWidth: 0.01,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
    borderTopRightRadius: 5,
    borderTopLeftRadius: 5,
    backgroundColor: '#fff',
    borderColor: '#fff',
  },
  clientImage: {
    width: wp('16%'),
    height: hp('15%'),
    aspectRatio: 1,
    borderColor: '#fff',
    resizeMode: 'contain',
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
    borderTopRightRadius: 5,
    borderTopLeftRadius: 5,
  },
  smallImage: {
    width: wp('3%'),
    height: hp('3%'),
    aspectRatio: 1,
  },
  map: {
    height: '100%',
  },
  // Callout bubble
  bubble: {
    flexDirection: 'column',
    alignSelf: 'flex-start',
    backgroundColor: '#fff',
    borderRadius: 6,
    borderColor: '#ccc',
    borderWidth: 0.5,
    padding: 15,
    width: 150,
  },
  // Arrow below the bubble
  arrow: {
    backgroundColor: 'transparent',
    borderColor: 'transparent',
    borderTopColor: '#fff',
    borderWidth: 16,
    alignSelf: 'center',
    marginTop: -32,
  },
  arrowBorder: {
    backgroundColor: 'transparent',
    borderColor: 'transparent',
    borderTopColor: '#007a87',
    borderWidth: 16,
    alignSelf: 'center',
    marginTop: -0.5,
    // marginBottom: -15
  },
  // Character name
  name: {
    fontSize: 16,
    marginBottom: 5,
  },
  // Character image
  image: {
    width: '80%',
    height: 80,
    borderWidth: 1,
  },
  searchimage: {
    width: 20,
    height: 20,
    alignSelf: 'center',
    tintColor: 'blue',
  },
});

export default DashboardScreen;
