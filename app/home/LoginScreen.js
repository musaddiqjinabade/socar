import {
  SafeAreaView,
  Linking,
  Alert,
  View,
  Dimensions,
  Text,
  ScrollView,
  TextInput,
  TouchableOpacity,
  Keyboard,
  KeyboardAvoidingView,
  Animated,
  StatusBar,
  ActivityIndicator,
  StyleSheet,
  Platform,
} from 'react-native';
import styles, {
  IMAGE_HEIGHT,
  IMAGE_HEIGHT_NEW,
  IMAGE_HEIGHT_SMALL,
} from '../styles/Common';
import styleConstants from '../styles/StyleConstants';
import LinearGradient from 'react-native-linear-gradient';
import {withNavigation} from 'react-navigation';
import {Container, Toast} from 'native-base';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  widthPercentageToDP,
  heightPercentageToDP,
} from 'react-native-responsive-screen';
import React from 'react';
const axios = require('axios');
import LoadingIndicator from '../core/components/LoadingIndicator';
import TabManager from './TabManager';
import auth from '@react-native-firebase/auth';
import database from '@react-native-firebase/database';

class LoginScreen extends React.Component {
  constructor(props) {
    super(props);
    this.imageHeightnnew = new Animated.Value(IMAGE_HEIGHT_NEW);
    this.imageHeight = new Animated.Value(IMAGE_HEIGHT);
    this.state = {
      agencyCode: null,
      emailAddress: null,
      password: null,
      loading: false,
      isFocused: false,
      token: '',
      userToken: '',
      platform: '',
      resetToken: '',
      resetAgency: '',
      resetPassword: '',
      resetConfirmPassword: '',
      user_data: false,
    };
  }

  async componentDidMount() {
    this.setState(
      {
        loading: true,
        emailAddress: null,
        password: null,
      },
      async () => {
        await AsyncStorage.getItem('user_token').then((value) => {
          var user_data = JSON.parse(value);
          console.log('userdata12', user_data);
          if (user_data && user_data.uid) {
            this.setState({user_data: true, loading: false});
          } else {
            this.setState({user_data: false, loading: false});
          }
        });
      },
    );
  }

  appWokeUp = (event) => {
    // this handles the use case where the app is running in the background and is activated by the listener...
    // Alert.alert('Linking Listener','url  ' + event.url)
    this.navigate(event.url);
  };

  handleOpenURL = (event) => {
    // D
    this.navigate(event.url);
  };

  showAlert = (title, message) => {
    Alert.alert(
      title,
      message,
      [{text: 'OK', onPress: () => console.log('OK Pressed')}],
      {cancelable: false},
    );
  };

  componentWillMount() {
    if (Platform.OS === 'ios') {
      this.setState({platform: 'ios'});
      this.keyboardWillShowSub = Keyboard.addListener(
        'keyboardWillShow',
        this.keyboardWillShow,
      );
      this.keyboardWillHideSub = Keyboard.addListener(
        'keyboardWillHide',
        this.keyboardWillHide,
      );
    } else {
      this.setState({platform: 'android'});
      this.keyboardWillShowSub = Keyboard.addListener(
        'keyboardDidShow',
        this.keyboardDidShow,
      );
      this.keyboardWillHideSub = Keyboard.addListener(
        'keyboardDidHide',
        this.keyboardDidHide,
      );
    }
  }

  componentWillUnmount() {
    this.keyboardWillShowSub.remove();
    this.keyboardWillHideSub.remove();
  }

  keyboardWillShow = (event) => {
    Animated.timing(this.imageHeight, {
      duration: event.duration,
      toValue: IMAGE_HEIGHT_SMALL,
    }).start();
  };

  keyboardWillHide = (event) => {
    Animated.timing(this.imageHeight, {
      duration: event.duration,
      toValue: IMAGE_HEIGHT,
      //toValue: IMAGE_HEIGHT_NEW
    }).start();
  };

  keyboardDidShow = (event) => {
    Animated.timing(this.imageHeight, {
      toValue: IMAGE_HEIGHT_SMALL,
      //toValue: IMAGE_HEIGHT_SMALL
    }).start();
  };

  keyboardDidHide = (event) => {
    Animated.timing(this.imageHeight, {
      toValue: IMAGE_HEIGHT,
      //toValue: IMAGE_HEIGHT_NEW
    }).start();
  };

  showMessage(message) {
    Toast.show({
      text: message,
      duration: 2000,
    });
  }

  isValidEmail = (text) => {
    var reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return reg.test(text);
  };

  handleFocus = (event) => {
    this.setState({isFocused: true});
    if (this.props.onFocus) {
      this.props.onFocus(event);
    }
  };

  handleBlur = (event) => {
    this.setState({isFocused: false});
    if (this.props.onBlur) {
      this.props.onBlur(event);
    }
  };

  async login() {
    this.setState({signloading: true}, async () => {
      var error = null;
      if (
        this.state.emailAddress === null ||
        this.state.emailAddress === '' ||
        !this.isValidEmail(this.state.emailAddress)
      ) {
        error = 'Please enter a valid email address';
        this.setState({signloading: false});
      } else if (
        this.state.password === null ||
        this.state.password === '' ||
        this.state.password.length < 8
      ) {
        error = 'Password must contain atleast 8 characters!';
        this.setState({signloading: false});
      } else if (this.state.password.length < 8) {
        error = 'Password must contain at least 8 characters!';
        this.setState({signloading: false});
      }
      if (error) {
        this.showMessage(error);
        this.setState({signloading: false});

        return;
      } else {
        await auth()
          .signInWithEmailAndPassword(
            this.state.emailAddress,
            this.state.password,
          )
          .then(async (user) => {
            console.log('user data after:', user);
            await AsyncStorage.setItem('user_token', JSON.stringify(user));
            this.props.navigation.navigate('InitialScreen');
          })
          .catch((error) => {
            if (error.code === 'auth/wrong-password') {
              console.log('That email address is already in use!');
              this.showMessage('[Please corrent password');
              this.setState({signloading: false});
            }
            this.showMessage('Somethimg Went Wrong');

            console.error(error);

            // this.showMessage(error);
            this.setState({signloading: false});
          });
      }
    });
  }

  render() {
    const {navigate} = this.props.navigation;
    const {onFocus, onBlur, ...otherProps} = this.props;
    if (this.state.loading) {
      return <LoadingIndicator />;
    } else {
      if (this.state.user_data) {
        return <TabManager />;
      } else {
        return (
          <SafeAreaView style={[styles.full, styles.backgroundColor]}>
            <Container style={{flex: 1}}>
              <StatusBar
                barStyle="dark-content"
                hidden={false}
                backgroundColor="transparent"
                translucent={false}
              />
              <ScrollView
                style={{flex: 1}}
                keyboardShouldPersistTaps={'always'}>
                <KeyboardAvoidingView
                  behavior={Platform.OS === 'ios' ? 'padding' : null}
                  style={[styles.full]}>
                  <View style={[styles.full, styles.center, styles.column]}>
                    <View
                      style={[
                        styles.container,
                        styles.column,
                        {
                          flex: 1,
                          justifyContent: 'center',
                          marginTop: widthPercentageToDP('3%'),
                        },
                      ]}>
                      <Animated.Image
                        source={require('../assets/images/store_logo.jpg')}
                        style={[
                          styles.logo,
                          {
                            width: widthPercentageToDP('55%'),
                            height: this.imageHeight,
                          },
                        ]}
                      />

                      <Text
                        style={[
                          styles.normalFont,
                          styles.fontSize(2.4),
                          {color: '#3985C4'},
                        ]}>
                        Sign in to access the account
                      </Text>
                    </View>
                  </View>
                  {/* <ScrollView style={{flex:1}}> */}
                  <View style={[styles.full, styles.column]}>
                    <View
                      style={[
                        styles.full,
                        styles.column,
                        styles.pd(4),
                        styles.mt(2),
                        {
                          flex: 2,
                          marginBottom: this.state.isFocused
                            ? widthPercentageToDP('6%')
                            : null,
                        },
                      ]}>
                      <View style={[styles.mb(2), styles.normalBorderBottom]}>
                        <Text style={[styles.normalFont, styles.fontSize(2)]}>
                          EMAIL ADDRESS
                        </Text>
                        <TextInput
                          autoCapitalize="none"
                          style={[
                            styles.pv(1),
                            styles.ph(0),
                            styles.fontSize(2.5),
                            {color: '#828ea5', fontFamily: 'Catamaran-Medium'},
                          ]}
                          placeholder={'Enter Your Email Address'}
                          placeholderTextColor="#828ea5"
                          onFocus={this.handleFocus}
                          onBlur={this.handleBlur}
                          onChangeText={(text) => {
                            this.setState({
                              emailAddress: text,
                              isFocused: true,
                            });
                          }}
                          value={this.state.email}
                        />
                      </View>
                      <View style={[styles.mb(2), styles.normalBorderBottom]}>
                        <Text style={[styles.normalFont, styles.fontSize(2)]}>
                          PASSWORD
                        </Text>
                        <TextInput
                          autoCapitalize="none"
                          secureTextEntry={true}
                          style={[
                            styles.pv(1),
                            styles.ph(0),
                            styles.boldFont,
                            styles.fontSize(2.5),
                            {color: '#828ea5', fontFamily: 'Catamaran-Medium'},
                          ]}
                          placeholder={'Enter Your Password'}
                          onFocus={this.handleFocus}
                          onBlur={this.handleBlur}
                          placeholderTextColor="#828ea5"
                          onChangeText={(text) => {
                            this.setState({
                              password: text,
                            });
                          }}
                        />
                      </View>
                      <View
                        style={[styles.full, styles.column, {borderRadius: 5}]}>
                        <TouchableOpacity
                          style={[styles.mb(2), styles.full]}
                          onPress={async () => {
                            //console.log("internet : ", await this.checkInternet());
                            // if (await this.checkInternet()) {
                            this.login();
                            // }
                            // else {
                            // this.showToast("No Internet Connectivity!")
                            // }
                          }}>
                          <LinearGradient
                            colors={[
                              styleConstants.gradientStartColor,
                              styleConstants.gradientEndColor,
                            ]}
                            // style={styles.linearGradient}
                            start={{x: 0.0, y: 0.25}}
                            end={{x: 1.2, y: 1.0}}
                            style={[
                              styles.linearGradient,
                              styles.rh(7),
                              styles.center,
                              ,
                              styles.br(0.3),
                              {
                                borderRadius: 5,
                                borderColor: '#fff',
                                shadowColor: '#000',
                                shadowOffset: {
                                  width: 0,
                                  height: 2,
                                },
                                shadowOpacity: 0.25,
                                shadowRadius: 3.84,
                                elevation: 5,
                              },
                            ]}>
                            {this.state.signloading === true ? (
                              <View
                                style={{
                                  flex: 1,
                                  flexDirection: 'row',
                                  alignItems: 'center',
                                  justifyContent: 'center',
                                }}>
                                <View
                                  style={{
                                    paddingRight: 10,
                                    backgroundColor: 'transparent',
                                  }}>
                                  <ActivityIndicator
                                    size={'small'}
                                    color="#FFFFFF"
                                  />
                                </View>
                                <View style={{backgroundColor: 'transparent'}}>
                                  <Text
                                    style={{
                                      color: '#FFFFFF',
                                      fontFamily: 'Catamaran-Bold',
                                    }}>
                                    Please Wait...
                                  </Text>
                                </View>
                              </View>
                            ) : (
                              <Text
                                style={[
                                  styles.boldFont,
                                  styles.fontSize(2.5),
                                  {color: 'white', textAlign: 'center'},
                                ]}>
                                {' '}
                                SIGN IN{' '}
                              </Text>
                            )}
                          </LinearGradient>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={[styles.mt(4), styles.full]}
                          onPress={() =>
                            this.props.navigation.replace('Registration')
                          }>
                          <Text
                            style={[
                              styles.boldFont,
                              styles.fontSize(2),
                              {
                                color: styleConstants.linkTextColor,
                                textAlign: 'center',
                              },
                            ]}>
                            Not Registered? Sign Up Here
                          </Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  </View>
                  <View style={[styles.column]}></View>
                  {/* </ScrollView> */}
                </KeyboardAvoidingView>
              </ScrollView>
            </Container>
          </SafeAreaView>
        );
      }
    }
  }
}

const screenStyles = StyleSheet.create({
  modal: {
    marginTop: widthPercentageToDP('3%'),
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    backgroundColor: 'rgba(0, 0, 0, 0)',
  },
  modal2: {
    maxHeight: 500,
    minHeight: 80,
  },
});

export default withNavigation(LoginScreen);
