import React from 'react';
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  StyleSheet,
  Platform,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import styles from '../styles/Common';
import styleConstants from '../styles/StyleConstants';
import DashboardScreen from './DashboardScreen';
import ChangePassword from '../Profile/ChangePassword';
import ProfileScreen from '../Profile/ProfileScreen';
import {Toast} from 'native-base';
import {withNavigation} from 'react-navigation';
import Cars from '../Cars';
const iconDashboard = require('../assets/images/dashboard.png');
const iconDashboardSelected = require('../assets/images/dashboard_selected.png');
const iconProfile = require('../assets/images/profile.png');
const iconProfile_Selected = require('../assets/images/profile_Selected.png');
const sports_car = require('../assets/images/sports_car.png');
const sports_car_Selected = require('../assets/images/sports_car_blue.png');

class TabManager extends React.Component {
  state = {
    selectedIndex: 0,
    expanded: true,
    data: '',
    date: new Date(),
    signloading: false,
  };

  async componentDidMount() {
    this.setState({selectedIndex: 0});
    await AsyncStorage.getItem('user_token').then((value) => {
      var user_data = JSON.parse(value);
      console.log('userdata::', user_data);
      if (user_data.BuyerId) {
        this.setState({user_data: user_data});
      }
    });
  }

  showToast(message) {
    Toast.show({
      text: message,
      duration: 2000,
    });
  }

  icons = [
    {
      label: 'Dashboard',
      normal: iconDashboard,
      selected: iconDashboardSelected,
    },
    {
      label: 'Cars',
      normal: sports_car,
      selected: sports_car_Selected,
    },
    {
      label: 'Rest Password',
      normal: iconProfile,
      selected: iconProfile_Selected,
    },
  ];

  getSelectedScreen() {
    if (this.state.selectedIndex === 0) {
      return <DashboardScreen />;
    } else if (this.state.selectedIndex === 1) {
      return <Cars />;
    } else if (this.state.selectedIndex === 2) {
      return <ChangePassword />;
    } else if (this.state.selectedIndex === 4) {
      return <ProfileScreen />;
    }
  }

  render() {
    const userData = null;
    const avatar_url = null;
    const Avatar_Data = null;
    return (
      <View style={[styles.full, styles.backgroundColor]}>
        <View style={{flex: 1}}>
          <View style={[styles.full]}>{this.getSelectedScreen()}</View>
        </View>

        <View
          style={[
            styles.rh(9),
            styles.row,
            styles.center,
            {
              backgroundColor: 'white',
              borderTopColor: '#e2e4e9',
              borderTopWidth: 0.2,
            }
          ]}>
          {this.icons.map((item, index) => {
            return (
              <TouchableOpacity
                key={index}
                onPress={async () => {
                  this.setState(
                    {
                      selectedIndex: index,
                    },
                    () => {
                      if (this.state.selectedIndex === 1) {
                        this.setState({date: new Date()});
                      }
                    },
                  );
                }}
                style={[styles.full, styles.column, styles.center]}>
                <Image
                  style={[styles.rh(3), styles.rw(5)]}
                  resizeMode="contain"
                  source={
                    this.state.selectedIndex === index
                      ? this.icons[index].selected
                      : this.icons[index].normal
                  }
                />
                <Text
                  style={[
                    styles.boldFont,
                    styles.fontSize(1.5),
                    {
                      color:
                        this.state.selectedIndex === index
                          ? styleConstants.linkTextColor
                          : styleConstants.inactiveLinkColor,
                    },
                  ]}>
                  {this.icons[index].label}
                </Text>
              </TouchableOpacity>
            );
          })}
          <TouchableOpacity
            activeOpacity={0.7}
            onPress={async () => {
              this.setState({
                selectedIndex: this.icons.length + 1,
              });
            }}
            style={[styles.full, styles.center, styles.pb(0.5)]}>
            <View
              style={[
                styles.br(0.5),
                {
                  justifyContent: 'center',
                  alignSelf: 'center',
                  borderColor: 'white',
                  shadowRadius: 10,
                  shadowColor: styleConstants.gradientStartColor,
                  elevation: 1,
                  // borderWidth:1
                },
              ]}>
              {Avatar_Data && Avatar_Data.avatar_url ? (
                <Image
                  style={[
                    styles.rh(10),
                    styles.rw(8),
                    styles.br(0.5),
                    {
                      aspectRatio: 1,
                      justifyContent: 'center',
                      alignSelf: 'center',
                      alignContent: 'center',
                      alignItems: 'center',
                      color: styleConstants.gradientStartColor,
                      // borderRadius: 25
                    },
                  ]}
                  resizeMode={'cover'}
                  resizeMethod={'resize'}
                  source={{uri: Avatar_Data.avatar_url}}
                />
              ) : (
                <View
                  style={[
                    // styles.rh(6),
                    // styles.rw(6),

                    {
                      aspectRatio: 1,
                      borderWidth: Platform.OS === 'ios' ? 1 : null,
                      borderColor: 'grey',
                      borderRadius: Platform.OS === 'ios' ? 5 : null,
                    },
                  ]}>
                  <Text
                    style={[
                      styles.normalFont,
                      styles.fs(4),
                      {
                        color: styleConstants.gradientStartColor,
                        justifyContent: 'center',
                        alignSelf: 'center',
                      },
                    ]}>
                    {this.state.user_data
                      ? this.state.user_data.FirstName.charAt(0).toUpperCase()
                      : 'userData.name'.charAt(0).toUpperCase()}
                  </Text>
                </View>
              )}
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
const mapStateToProps = (state) => ({
  data: state.user.data,
});

const style = StyleSheet.create({
  modal: {
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    backgroundColor: 'rgba(0, 0, 0, 0)',
  },
  modal1: {
    maxHeight: 315,
    minHeight: 80,
  },
});

export default withNavigation(TabManager);
