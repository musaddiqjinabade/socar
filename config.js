import firebase from '@react-native-firebase/app';
// import '@firebase/auth';
// import '@firebase/firestore';

const firebaseConfig = {
  apiKey: 'AIzaSyBTIbw2MS_cQQIkBsUQxWvw4soPIajMHuM',
  authDomain: 'socar-c538c.firebaseapp.com',
  databaseURL: 'https://socar-c538c-default-rtdb.firebaseio.com',
  projectId: 'socar-c538c',
  storageBucket: 'socar-c538c.appspot.com',
  messagingSenderId: '185224780570',
  appId: '1:185224780570:web:8347e93d64e544dfdb6aa3',
  measurementId: 'G-3J0CSHBHDN',
};
if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

export {firebase};
